# IA-tools #

This is repository of IA tools mainly for parsing and building back XML files of workflow configuration.

### INTRO What is this all about? ###

* Version: 0.1 beta

Tools supports your IA development and with combination of git repository on all outputs you can track all workflow
config changes. With building functionality it opens a huge space for code promotion (deployment) auto process which
is very important. At the end you could do the development in your IDE not directly in IA.

### PACKAGING? ###

To create a package run assembly:single


### INSTALL How to set up? ###

TODO

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

Before you start contributing try to contact the owner and align your ideas and development with him.

* Test the changed
* Do not push code if not working

### CONTACTS ###

* Owner: david.chobotsky at gmail.com

# RELEASE and PLAN #

### version 0.2 beta ###
 * install - create a package easy to use/import
 * doc - create a guide
 * dev - improve a fake classes
 * dev - add class attributes into the fake class
 * parser - start parsing win process scripts
 * parser - parse full modules info
 * implement process to auto imports commited changes

### version 0.1 beta ###
 * X builder - clean up and parametrize builder class
 * X builder - provide atomic builder back
 * X test - create test class(es) and remove manual run
 * X create MAIN to run parser and builder
 * X define globals
 * X provide param inputs
 * X define calls form cmd
 * X create batch file to run auto IA export+parser

### NICE TO HAVE ###
 * structure by subworkflow & queue
