/**
 * Created by dchobotsky on 11/26/2015.
 */
import groovy.sql.Sql

class Connector {

    def connect(def userid, def encryptedPwd, def job) {

        def db = [
                url: 'jdbc:sqlserver://SRV374CZE:1433\\DocumentSmart-IA',
                user:'mba',
                pass:'mbapass',
                driver:'com.microsoft.sqlserver.jdbc.SQLServerDriver'
        ]

        Sql sql = Sql.newInstance(db.url, db.user, db.pass, db.driver);
        println("DB connection established.");
        def result = sql.firstRow("SELECT * FROM db_info");
        println(result.toString());

        return sql;
    }
}
