/**
 * Created by dchobotsky on 11/24/2015.
 */
class ExtendedProperties extends Properties {
    private final int MAX_JUMPS = 10

    /**
     * Extended method for reference feature in properties file
     *
     * A key within a value is enclosed like that ${ }
     *
     * Example:
     *  key=${key2}
     *  key2='My Value'
     *
     * @param key
     * @return value
     */
    @Override
    public String getProperty(String key) {
        String value = super.getProperty(key);
        if (!value) return null

        int numberOfJumps = 0;
        while (value.startsWith('${')) {
            String newKey = value.substring(2, value.length() - 1);
            value = super.getProperty(newKey);

            numberOfJumps++;
            if (numberOfJumps.equals(MAX_JUMPS)) throw new Exception('[ERROR] There is possible cycle in your property references! Check the definition.');
        }

        return value;
    }
}