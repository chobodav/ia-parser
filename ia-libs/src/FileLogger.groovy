/**
 * Created by dchobotsky on 11/24/2015.
 */

import java.text.SimpleDateFormat;

class FileLogger {
    private final LOG_FILE_PATH = server.'system-logfile'

    /**
     * Function for writing messages to log file
     *
     * @param severity
     * @param filePath
     * @param message
     */
    public void log(int severity, String message, String filePath = LOG_FILE_PATH) {
        File file = new File(filePath);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String msglevel
        switch (severity) {
            case 1: msglevel = 'ERROR'
                break
            case 2: msglevel = 'WARN'
                break
            case 3: msglevel = 'INFO'
                break
            case 4: msglevel = 'DEBUG'
                break
            default: msglevel = 'UNKNOWN'
                break
        }

        try {
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(dateFormat.format(new Date()));
            bw.write(" [" + msglevel + "] ");
            bw.write(message);
            bw.write("\r\n");

            bw.flush();
        } catch (IOException e) {
            println("EXCEPTION during writing to error.log file > " + e);
        } finally {
            if (bw != null) {
                bw.close();
            }
        }
    }
}
