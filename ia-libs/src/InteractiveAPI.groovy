/**
 * Created by dchobotsky on 02/11/2015.
 */
import groovy.xml.MarkupBuilder

class InteractiveAPI {

    Writer writer
    MarkupBuilder builder

    final private String USER = ''//server.'system-account'
    final private String PASS = ''//server.'system-password'
    final private def ENDPOINTS = [
            Ticket:'/ticket-ws/ticket.wsdl',
            FileTicket:'/fileTicket-ws/fileTicket.wsdl'
//            Template:'/template-ws/template.wsdl',
    ]

    InteractiveAPI() {
        writer = new StringWriter()
        builder = new MarkupBuilder(writer)

        builder.getMkp().xmlDeclaration(version: '1.0', encoding: 'UTF-8');
    }

    public String call(String method, Map<String, String> data) {
        builder.'soapenv:Envelope'('xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'xmlns:int': 'http://www.gmc.net/Phoenix/Integration') {
            'soapenv:Header'() {
                'wsse:Security'('soapenv:mustUnderstand': '1', 'xmlns:wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd') {
                    'wsse:UsernameToken'('wsu:Id': 'UsernameToken-2', 'xmlns:wsu': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd') {
                        'wsse:Username'(USER)
                        'wsse:Password'('Type': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText', PASS)
                    }
                }
            }
            'soapenv:Body'() {
                switch (method) {
                    case 'ping':
                        callPing()
                        break
                    case 'createTicket':
                        callCreateTicket(data)
                        break
                    case 'createFileTicket':
                        callCreateFileTicket(data)
                        break
//                    case 'createStandaloneTicket':
//                        callCreateStandaloneTicket(data.'body')
//                        break
                    case 'createTicketSnapshot':
                        callCreateTicketSnapshot(data)
                        break
                    case 'deleteTicket':
                        callDeleteTicket(data)
                        break
                    case 'deleteFileTicket':
                        callDeleteFileTicket(data)
                        break
                    case 'exportTicket':
                        callExportTicket(data)
                        break
                    case 'getTicketRights':
                        callGetTicketRights(data)
                        break
                    case 'listTickets':
                        callListTickets(data)
                        break
                    case 'listFileTickets':
                        callListFileTickets(data)
                        break
                    case 'setTicketRights':
                        callSetTicketRights(data)
                        break
                    case 'updateTicketData':
                        callUpdateTicketData(data)
                        break
                    case 'updateFileTicketData':
                        callUpdateFileTicketData(data)
                        break
                    case 'validateTicket':
                        callValidateTicket(data)
                        break
                    default:
                        throw new Exception(this.class.name + ': Unknown call method! Available are ping,createTicket,createFileTicket,createTicketSnapshot,deleteTicket,deleteFileTicket,exportTicket,getTicketRights,setTicketRights,listTickets,listFileTickets,updateTicketData,updateFileTicketData,validateTicket.')
                        break
                }
            }
        }

        return writer.toString()
    }

    public String getEndpoint(String object) {
        switch (object) {
            case 'Ticket':
                return ENDPOINTS.Ticket
            case 'FileTicket':
                return ENDPOINTS.FileTicket
            default:
                throw new Exception(this.class.name + ': Unknown object and endpoint!')
        }
    }

    /**
     *
     * @return String - SOAP Request Body
     *
     * Example:
        def request = ic.call('ping')
     */
    private String callPing() {
        return builder.'int:PingRequest'()
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
     Id
     Documents
        Template
        Description     OPTIONAL
        DataInputs      OPTIONAL
            ModuleName
            MailMerge
            BinaryData  > ONE OF OPTIONS
            Location    > ONE OF OPTIONS
        Commands        OPTIONAL
     MultipleRecord     OPTIONAL
     Company            OPTIONAL
     Properties         OPTIONAL
     Contract
     State
     UserName           OPTIONAL
     Role               > ONE OF OPTIONS
     SystemData         OPTIONAL
     MessageVersion     OPTIONAL

     * Example:
        def document = [
            Template   : 'companyRoot:S:Production://Templates/MBA_PeriodReport.jld',
            Description: '',
            DataInputs : [
                [
                    ModuleName: '',
                    BinaryData: '',
                    Location  : '',
                ],
            ],
            Commands   : [
                '-CLIENT_IDParametersDataMaster': 'C11166',
            ],
        ]

        def data = [
            Documents     : [document],
            SystemData    : '',
            MessageVersion: 0.1,
            MultipleRecord: false,
            Company       : 'MBA',
            Properties    : [
                icon       : 'companyRoot://WebResources/TicketIcons/doc_01.png',
                title      : '',
                description: '',
                priority   : 3,
            ],
            Attachments   : [
                'test path',
            ],
            Contract      : [
                Id          : '999',
                CustomerName: 'WS TEST',
            ],
            State         : 'S_simple_scenario_writer_assigned',
            UserName      : 'writer',
            Role          : 'writer',
        ]

         def request = ic.call('createTicket', data)
     */
    private String callCreateTicket(Map<String, String> data) {
        checkInputData(['Documents', 'Contract', 'State'], data)
        if (!data.'UserName' && !data.'Role') throw new Exception('[ERROR] ' + this.class.name + ' - NOR USER NEITHER ROLE in request. You must define user/role in UserName/Role: value')

        String body = builder.'int:CreateTicket2Request'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            if (data.'MessageVersion') 'int:MessageVersion'(data.'MessageVersion')
            'int:Ticket'() {
                data.'Documents'.each { doc ->
                    'int:Document'() {
                        if (!doc.'Template') throw new Exception('[ERROR] ' + this.class.name + ' - NO TEMPLATE NAME in request. You must define Template: value')
                        'int:Template'(doc.'Template')
                        if (doc.'Description' != null) 'int:Description'(doc.'Description')
                        doc.'DataInputs'.each { i ->
                            'int:DataDefinition'() {
                                if (data.'MailMerge') {
                                    'int:MailMerge'(i.'MailMerge')
                                } else {
                                    'int:ModuleName'(i.'ModuleName')
                                }
                                'int:Source'() {
                                    if (data.'BinaryData') {
                                        'int:BinaryData'(i.'BinaryData')
                                    } else {
                                        'int:Location'(i.'Location')
                                    }
                                }
                            }
                        }
                        doc.'Commands'.each { c ->
                            'int:Command'() {
                                'int:CommandName'(c.key)
                                'int:CommandValue'(c.value)
                            }
                        }
                    }
                }
                if (data.'MultipleRecord') 'int:MultipleRecord'(data.'MultipleRecord')
                if (data.'Company') 'int:Company'(data.'Company')
                if (data.'Properties' && data.'Properties'.size() > 0) {
                    'int:Properties'() {
                        data.'Properties'.each { key, value ->
                            'int:Property'(Name: key, value)
                        }
                    }
                }
                if (data.'Attachments' && data.'Attachments'.size() > 0) {
                    'int:Attachments'() {
                        data.'Attachments'.each { a ->
                            'int:AttachmentName'(a)
                        }
                    }
                }
                'int:Contract'() {
                    data.'Contract'.each { key, value ->
                        "int:$key"(value)
                    }
                }
                'int:State'(data.'State')
                'int:Holder'() {
                    if (data.'UserName') {
                        'int:UserName'(data.'UserName')
                    } else {
                        'int:Role'(data.'Role')
                    }
                }
            }
        }

        return body
    }



    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
     SystemData         OPTIONAL
     FileName
     Title
     State
     Company            OPTIONAL
     UserName           > ONE OF OPTIONS
     Role               > ONE OF OPTIONS
     Contract
     Properties         OPTIONAL

     def data = [
        SystemData    : '',
        FileName      : 'path',
        Title         : 'title',
        State         : 'S_simple_scenario_writer_assigned',
        Company       : 'MBA',
        UserName      : 'writer',
        Role          : 'writer',
        Contract      : [
            Id          : '999',
            CustomerName: 'WS TEST',
        ],
        Properties    : [
            icon       : 'companyRoot://WebResources/TicketIcons/doc_01.png',
            title      : '',
            description: '',
            priority   : 3,
        ],
     ]

     def request = ic.call('createFileTicket', data)
     */
    private String callCreateFileTicket(Map<String, String> data) {
        checkInputData(['FileName', 'Title', 'State', 'Contract'], data)
        if (!data.'UserName' && !data.'Role') throw new Exception('[ERROR] ' + this.class.name + ' - NOR USER NEITHER ROLE in request. You must define user/role in UserName/Role: value')

        String body = builder.'int:CreateFileTicketRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:FileTicket'() {
                'int:FileName'(data.'FileName')
                'int:Title'(data.'Title')
                'int:State'(data.'State')
                if (data.'Company') 'int:Company'(data.'Company')
                'int:Holder'() {
                    if (data.'UserName') {
                        'int:UserName'(data.'UserName')
                    } else {
                        'int:Role'(data.'Role')
                    }
                }
                'int:Contract'() {
                    data.'Contract'.each { key, value ->
                        "int:$key"(value)
                    }
                }
                if (data.'Properties' && data.'Properties'.size() > 0) {
                    'int:Properties'() {
                        data.'Properties'.each { key, value ->
                            'int:Property'(Name: key, value)
                        }
                    }
                }
            }
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
        Id
        Label              OPTIONAL
        Message            OPTIONAL
        UserName           OPTIONAL
        SystemData         OPTIONAL

     * Example:
     def request = ic.call('createTicketSnapshot', [Id: 4])

     */
    private String callCreateTicketSnapshot(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:CreateTicketSnapshotRequest'() {
            'int:TicketSnapshot'() {
                'int:Id'(data.'Id')
                if (data.'Label') 'int:Label'(data.'Label')
                if (data.'Message') 'int:Message'(data.'Message')
                if (data.'UserName') 'int:UserName'(data.'UserName')
            }
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     * Example:
        def request = ic.call('deleteTicket', [Id: 4])

     -- or --

        def data = [Id: '4']
        def request = ic.call('deleteTicket', data)
     */
    private String callDeleteTicket(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:DeleteTicketRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            if (data.'MessageVersion') 'int:MessageVersion'(data.'MessageVersion')
            'int:Id'(data.'Id')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     * Example:
        def request = ic.call('deleteFileTicket', [Id: 4])

     -- or --

        def data = [Id: '4']
        def request = ic.call('deleteFileTicket', data)
     */
    private String callDeleteFileTicket(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:DeleteFileTicketRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:Id'(data.'Id')
        }

        return body
    }

    /**
     *
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
        Id
        SystemData          OPTIONAL
        Script              OPTIONAL
            Name
            Properties      OPTIONAL
        Engine              > ONE OF OPTIONS
        SendToIA            > ONE OF OPTIONS
            ServerName      OPTIONAL
            Queue
        ProductionArchive   > ONE OF OPTIONS
            MetaTemplate    OPTIONAL
        ResponseType        OPTIONAL

     * Example:
        def datas = [
            Id               : 4,
            Script           : [
                Name      : 'ScriptName',
                Properties: [
                    title      : 'property 1',
                    description: 'property 2',
                ],
            ],
        Engine           : 'engine',
        SendToIA         : [
            ServerName: 'IAserver',
            Queue     : 'IAqueue',
        ],
        ProductionArchive: [
            MetaTemplate: 'metaTemplate',
        ],
        ResponseType     : 'Byte',

        def request = ic.call('exportTicket', data)
     */
    private String callExportTicket(Map<String, String> data) {
        checkInputData(['Id'], data)
        if (!data.'Engine' && !data.'SendToIA' && !data.'ProductionArchive')
            throw new Exception('[ERROR] ' + this.class.name + ' - NO OPTION SELECTED in request. You must define one from these Engine/SendToIA/ProductionArchive.')

        String body = builder.'int:ExportTicketRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:Id'(data.'Id')
            if (data.'Script') {
                'int:Script'() {
                    'int:Name'(data.'Script'.'Name')
                    if (data.'Script'.'Properties' && data.'Script'.'Properties'.size() > 0) {
                        'int:Properties'() {
                            data.'Script'.'Properties'.each { key, value ->
                                'int:Property'(Name: key, value)
                            }
                        }
                    }
                }
            }
            if (data.'Engine') {
                'int:Engine'(data.'Engine')
            } else if (data.'SendToIA') {
                'int:SendToIA'() {
                    if (data.'SendToIA'.'ServerName') 'int:ServerName'(data.'SendToIA'.'ServerName')
                    'int:Queue'(data.'SendToIA'.'Queue')
                }
            } else if (data.'ProductionArchive') {
                'int:ProductionArchive'() {
                    if (data.'MetaTemplate') 'int:MetaTemplate'(data.'MetaTemplate')
                }
            }
            if (data.'ResponseType') 'int:ResponseType'(data.'ResponseType')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     * Example:
        def request = ic.call('getTicketRights', [Id: 4])

     -- or --

        def data = [Id: '4']
        def request = ic.call('getTicketRights', data)
     */
    private String callGetTicketRights(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:DeleteTicketRequest'() {
            if (data.'SystemData' != null) 'int:SystemData'(data.'SystemData')
            if (data.'MessageVersion' != null) 'int:MessageVersion'(data.'MessageVersion')
            'int:Id'(data.'Id')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
        Company         OPTIONAL
        SystemData      OPTIONAL

     * Example:
        def request = ic.call('listTickets', [])
        def request = ic.call('listTickets', [Company: 'MBA'])

     -- or --

        def data = [Company: 'MBA']
        def request = ic.call('listTickets', [])
     */
    private String callListTickets(Map<String, String> data) {
        String body = builder.'int:ListTicketsRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            if (data.'Company') 'int:Company'(data.'Company')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
        Company         OPTIONAL
        SystemData      OPTIONAL

     * Example:
        def request = ic.call('listFileTickets', [])
        def request = ic.call('listFileTickets', [Company: 'MBA'])

     -- or --

        def data = [Company: 'MBA']
        def request = ic.call('listFileTickets', [])
     */
    private String callListFileTickets(Map<String, String> data) {
        String body = builder.'int:ListFileTicketsRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            if (data.'Company') 'int:Company'(data.'Company')
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
     Id
     Users          OPTIONAL
        Name
        Permissions
     Groups         OPTIONAL
        Name
        Permissions

     * Example:
        def user/group = [
                     Name       : 'user1',
                     Permissions: ['one','two',]
             ]
        def data = [
            Id    : 4,
            Users : [user],
            //Groups: [group]
     ]

     def request = ic.call('setTicketRights', data)
     */
    private String callSetTicketRights(Map<String, String> data) {
        checkInputData(['Id'], data)
        if (!data.'Users' && !data.'Groups') throw new Exception('[ERROR] ' + this.class.name + ' - NOT DEFINED USER/GROUP in request. You must define at least one user or group.')

        String body = builder.'int:SetTicketRightsRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            if (data.'MessageVersion') 'int:MessageVersion'(data.'MessageVersion')
            'int:Id'(data.'Id')
            'int:Rights'() {
                data.'Users'.each { u ->
                    'int:User'() {
                        'int:Name'(u.'Name')
                        u.'Permissions'.each { p ->
                            'int:Permission'(p)
                        }
                    }
                }
                data.'Groups'.each { g ->
                    'int:Group'() {
                        'int:Name'(g.'Name')
                        g.'Permissions'.each { p ->
                            'int:Permission'(p)
                        }
                    }
                }
            }
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
     SystemData
     Id
     DocumentIndex
     DocumentData
     DataInputs
     Commands

     * Example:
        def data = [
            Id    : 4,
            SystemData : 'name',
            DocumentIndex: 'cid:141192000379'
            DataInputs : [
                [
                ModuleName: '',
                MailMerge : '',
                BinaryData: '',
                Location  : '',
                ],
            ],
            Commands   : [
                '-CLIENT_IDParametersDataMaster': 'C11166',
            ],
        ]

     def request = ic.call('setTicketRights', data)
     */
    private String callUpdateTicketData(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:UpdateTicketDataRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:Id'(data.'Id')
            if (data.'DocumentIndex') 'int:DocumentIndex'(data.'DocumentIndex')
            'int:DocumentData'() {
                data.'DataInputs'.each { i ->
                    'int:DataDefinition'() {
                        if (data.'MailMerge') {
                            'int:MailMerge'(i.'MailMerge')
                        } else {
                            'int:ModuleName'(i.'ModuleName')
                        }
                        'int:Source'() {
                            if (data.'BinaryData') {
                                'int:BinaryData'(i.'BinaryData')
                            } else {
                                'int:Location'(i.'Location')
                            }
                        }
                    }
                }
                data.'Commands'.each { c ->
                    'int:Command'() {
                        'int:CommandName'(c.key)
                        'int:CommandValue'(c.value)
                    }
                }
            }
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Data:
     Id
     FileName
     BinaryData

     * Example:
        def user/group = [
                     Name       : 'user1',
                     Permissions: ['one','two',]
             ]
        def data = [
            Id    : 4,
            FileName : 'name',
            BinaryData: 'cid:141192000379'
     ]

     def request = ic.call('setTicketRights', data)
     */
    private String callUpdateFileTicketData(Map<String, String> data) {
        checkInputData(['Id', 'FileName', 'BinaryData'], data)

        String body = builder.'int:UpdateFileTicketDataRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:FileTicket'() {
                'int:Id'(data.'Id')
                'int:FileName'(data.'FileName')
                'int:BinaryData'(data.'BinaryData')
            }
        }

        return body
    }

    /**
     *
     * @param data
     * @return String - SOAP Request Body
     *
     * Required data: Id
     * Data:
        Id

     * Example:
        def request = ic.call('validateTicket', [Id: 4])

     -- or --

        def data = [Id: '4']
        def request = ic.call('validateTicket', data)
     */
    private String callValidateTicket(Map<String, String> data) {
        checkInputData(['Id'], data)

        String body = builder.'int:ValidateTicketRequest'() {
            if (data.'SystemData') 'int:SystemData'(data.'SystemData')
            'int:Id'(data.'Id')
        }

        return body
    }

    /**
     *
     * @param keyList
     * @param data
     */
    private void checkInputData(def keyList, Map<String, String> data) {
        def failed = false

        keyList.each { key ->
            if (!data."$key") {
                failed = true
                println "-- $key is required"
            }
        }

        if (failed) throw new Exception('[ERROR] ' + this.class.name + ' - INVALID DATA in request. You must define correct data input. Check a documentation.')
    }

    public static void main(String[] args) {
        InteractiveAPI ic = new InteractiveAPI()

//        def request = ic.call('ping')
//        def request = ic.call('deleteTicket', [Id: 4])
//        def request = ic.call('getTicketRights', [Id: 4])
//        def request = ic.call('createTicketSnapshot', [Id: 4])
//        def request = ic.call('listTickets', [Company: 'MBA'])
//        def request = ic.call('validateTicket', [Id: 4])

//        def document = [
//                Template   : 'companyRoot:S:Production://Templates/MBA_PeriodReport.jld',
//                Description: '',
//                DataInputs : [
//                        [
//                                ModuleName: '',
//                                BinaryData: '',
//                                Location  : '',
//                        ],
//                ],
//                Commands   : [
//                        '-CLIENT_IDParametersDataMaster': 'C11166',
//                ],
//        ]
//
//        def data = [
//                Documents     : [document],
//                SystemData    : '',
//                MessageVersion: 0.1,
//                MultipleRecord: false,
//                Company       : 'MBA',
//                Properties    : [
//                        icon       : 'companyRoot://WebResources/TicketIcons/doc_01.png',
//                        title      : '',
//                        description: '',
//                        priority   : 3,
//                ],
//                Attachments   : [
//                        'test path',
//                ],
//                Contract      : [
//                        Id          : '999',
//                        CustomerName: 'WS TEST',
//                ],
//                State         : 'S_simple_scenario_writer_assigned',
//                UserName      : 'writer',
//                Role          : 'writer',
//        ]
//
//        def request = ic.call('createTicket', data)

        def user = [
                Name       : '',
                Permissions: [
//                        'title',
                        'description',
                ]
        ]
        def group = [
                Name       : '',
                Permissions: [
                        'title',
                        'description',
                ]
        ]

        def data = [
                Id    : 4,
                Users : [user],
//                Groups: [group],
        ]

        def request = ic.call('setTicketRights', data)


        println request



        println ic.getEndpoint('Ticket')
    }
}

