/**
 * Created by dchobotsky on 12/21/2015.
 */
import groovy.sql.*
import java.util.*

class SqlPoolConstants {
    static long DB_CONN_WAIT_TIMEOUT = 1000 * 60 * 3    // How long to wait for free connection (milliseconds)
    static long DB_CONN_IDLE_TIMEOUT = 1000 * 60      // How long should free connections idle in the pool. After this timeout the free connection is closed.
    static int DB_POOL_MAX_CONNECTIONS = 2            // Maximum number of cached connections for each database.
    static def DATABASES = [
            'db1': [
                    'jdbc:jtds:sqlserver://SRV374CZE:1433/DocumentSmart-IA',
                    'mba',
                    'mbapass',
                    'net.sourceforge.jtds.jdbc.Driver'
            ],
            'db2': [
                    'jdbc:jtds:sqlserver://[DBServer];database=[DBName]',
                    '[Username]',
                    '[Password]',
                    'net.sourceforge.jtds.jdbc.Driver'
            ]

    ]
}

/**
 * Acquires connection to DB, runs given closure and frees
 * the connection automatically.
 */
static void runSql(String dbAlias, Closure closure) {
    Sql sql = SqlPoolImpl.instance.getConnection(dbAlias)
    try {
        closure.call(sql)
    } finally {
        SqlPoolImpl.instance.freeConnection(sql)
    }
}

// ----------------------------------------------------------------
// Implementation classes. Shouldn't be used from
// outside of this script.
// ----------------------------------------------------------------

@Singleton(lazy = true, strict = false)
class SqlPoolImpl {

    private def _pools = [:]
    private def _sqlToConnInfo = [:]
    private def _timer = new Timer()

    SqlPoolImpl() {
        for (e in SqlPoolConstants.DATABASES) {
            _pools.put(e.key, new DbPool(e.value));
        }
    }

    private Sql getConnection(String dbAlias) {
        def pool
        synchronized (_pools) {
            pool = _pools[dbAlias]
            if (pool == null) {
                throw new Exception("Unknown database alias '" + dbAlias + "'")
            }
        }
        ConnInfo ci = pool.getConnection()
        synchronized (_sqlToConnInfo) {
            _sqlToConnInfo.put(ci._sql, ci)
        }
        return ci._sql
    }

    private void freeConnection(Sql sql) {
        ConnInfo ci = null
        synchronized (_sqlToConnInfo) {
            ci = _sqlToConnInfo.remove(sql);
        }
        if (ci == null) {
            return;
        }
        ci._dbPool.freeConnection(ci);
    }

    private def close() {
        log("Closing SqlPool")
        _timer.cancel();
        synchronized (_pools) {
            for (e in _pools) {
                e.value.close()
            }
        }
    }

    public static log(String msg) {
        net.gmc.lib.logging.Logger.debug(SqlPool.class, msg, null)
    }

}

class ConnInfo {
    Sql _sql;
    DbPool _dbPool;
    ReleaseConnTimerTask _timerTask;
}

class ReleaseConnTimerTask extends TimerTask {
    ConnInfo _conInfo;

    public ReleaseConnTimerTask(ConnInfo conInfo) {
        _conInfo = conInfo
    }

    public void run() {
        _conInfo._dbPool.removeConnection(_conInfo)
    }
}

class DbPool {
    def _dbInfo;
    def _freeConnections = [];
    def _usedConnections = [];
    boolean _closed

    public DbPool(List dbInfo) {
        _dbInfo = dbInfo
    }

    /**
     *  Completely closes and removes connection (Is used with Timer
     *  to release idle connections)
     */
    synchronized void removeConnection(ConnInfo conInfo) {
        // check if really removed. It could be that
        // somebody acquired the connection in meantime
        // in which case we don't remove it.
        if (_freeConnections.remove(conInfo)) {
            SqlPoolImpl.log("Closing and removing connection " + conInfo._sql)
            try {
                conInfo._sql.close();
            } catch (Exception e) {
            }
            conInfo._timerTask = null;
        }
    }

    synchronized ConnInfo getConnection() {
        if (_closed) {
            throw new Exception("DB Pool is closed");
        }
        long timeout = System.currentTimeMillis() + SqlPoolConstants.DB_CONN_WAIT_TIMEOUT
        while (true) {

            // if some connection in cache
            if (!_freeConnections.isEmpty()) {
                // free connection available in cache, reuse it.
                def connInfo = _freeConnections.remove(0)
                _usedConnections.add(connInfo)
                SqlPoolImpl.log("Reusing connection " + connInfo._sql)
                if (connInfo._timerTask != null) {
                    connInfo._timerTask.cancel()
                    connInfo._timerTask = null
                }
                return connInfo
            }

            // no free connection in cache. Check if we can create a new one
            if ((_freeConnections.size() + _usedConnections.size()) < SqlPoolConstants.DB_POOL_MAX_CONNECTIONS) {
                // we can allocate new connection
                ConnInfo connInfo = new ConnInfo()
                SqlPoolImpl.log("Connecting to " + _dbInfo[0])
                connInfo._sql = Sql.newInstance(_dbInfo[0], _dbInfo[1], _dbInfo[2], _dbInfo[3])
                SqlPoolImpl.log("Connected to " + _dbInfo[0] + " - connection " + connInfo._sql)
                connInfo._dbPool = this
                _usedConnections.add(connInfo)
                return connInfo
            } else {
                // wait a little bit
                if (System.currentTimeMillis() > timeout) {
                    throw new Exception("Timeout when waiting for free connection to " + _dbInfo[0])
                }
                this.wait(100)
            }
        }
    }

    synchronized void freeConnection(ConnInfo ci) {
        _freeConnections.add(ci);
        _usedConnections.remove(ci);
        ReleaseConnTimerTask task = new ReleaseConnTimerTask(ci);
        ci._timerTask = task;
        SqlPoolImpl.log("Planning to remove connection " + ci._sql + " at " + new Date(System.currentTimeMillis() + SqlPoolConstants.DB_CONN_IDLE_TIMEOUT))
        SqlPoolImpl.instance._timer.schedule(task, SqlPoolConstants.DB_CONN_IDLE_TIMEOUT);
    }

    synchronized void close() {
        // What to do with used connections? We preserve it since
        // when closing the SqlPool (on script library recompile)
        // some executors can be running
        for (ci in _freeConnections) {
            try {
                SqlPoolImpl.log("Closing connection " + ci._sql)
                ci._sql.close()
            } catch (Exception ex) {
            }
        }
    }
}

// This is supported from PA 7.1.11.0
static void onScriptClose() {
    SqlPoolImpl.instance.close()
}


/*
import db.SqlPool; //X

String query = "INSERT INTO table (id,job,status) VALUES (?,?,?) ";

ArrayList values = new ArrayList();
values.add("[ID]");
values.add("[JOB]");
values.add("[STATUS]");

SqlPool.runSql("db1") { sql ->
    //sql.execute(query, values)
    def result = sql.firstRow("SELECT * FROM db_info");
    //def result = sql.firstRow("SELECT * FROM [DocumentSmart-IA].[dbo].[db_info]");
    println(result.toString());
}
*/