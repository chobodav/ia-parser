/**
 * Created by dchobotsky on 11/25/2015.
 */
import java.util.Properties;

def jobDocuments = job.getDocuments();
def logFilePath = server."app-log";
//def errorWriter = new logging.ErrorHandler();
def filePath = "C:\\MBASmart\\DocumentSmart V2\\Storage\\IA\\config\\.environment";

FileInputStream inputStream = null;
Properties properties = new Properties();

try {
    inputStream = new FileInputStream(filePath);
    properties.load(inputStream);

    // SAMPLE
    server.'environment-letter' = properties.getProperty("common_env_prefix")

    String environment = properties.getProperty("common_env_prefix");
    server.setAttribute("environment-letter", environment);

    // Update include list values for inputs according environment
    def values = [];
    values.add(environment + "*.xrx");
    values.add(environment + "*.afp");
    values.add(environment + "*.ps");

    def inputPostCompWatch = getObject("input", "PostComp_Watch");
    def inputAdeptOutput = getObject("input", "Adept_Output");

    if(inputPostCompWatch."enabled".equals("true")) {
        inputPostCompWatch.disable();
    }
    inputPostCompWatch."include-list" = values;

    def valuesAdept = [];
    valuesAdept.add(environment + "*.pdf");
    valuesAdept.add(environment + "*.pdf.trg");

    if(inputAdeptOutput."enabled".equals("true")) {
        inputAdeptOutput.disable();
    }
    inputAdeptOutput."include-list" = valuesAdept;

} catch (IOException e) {
    job."af-dpbm-message" = "During reading environment properties > " + e;
    job."af-dpbm-message-type" = "DBPM_JOB_ERROR";
    errorWriter.writeToLog(logFilePath, job."af-dpbm-message-type", job."af-dpbm-message");
    job.cancel(job."af-dpbm-message");
} finally {
    if (inputStream != null) {
        inputStream.close();
    }
}