import builder.ConfigBuilder
import parser.ConfigParser

/**
 * Created by d.chobotsky on 22.5.2015.
 */
class Main {
    /*
    CMD: ~\ia-tools-1.0-SNAPSHOT>groovy Main parse "c:\GMC\Apps\dev-IA\AmFamSolution\WorkflowBackups\AmFam_IA.xml"
    */

    public static void main(String[] args) {
        if (args.length.equals(0)) {
            File f = new File('./resources/cmdHelp.txt')
            println f.text
            System.exit(0)
        }
        if (args == null || args.length < 2) {
            System.err.println("Expecting at least one argument with path the file with configuration workflow or output file.")
            System.exit(1)
        }
        String inMethod = args[0]
        String xmlConfigFile = args[1]
        println 'method: ' + inMethod + ' > file: ' + xmlConfigFile

        switch (inMethod) {
            case 'parse':
                ConfigParser configParser = new ConfigParser(xmlConfigFile)
                configParser.run()
                break
            case 'build':
                ConfigBuilder configBuilder = new ConfigBuilder(xmlConfigFile)
                if (args.length.equals(3)) {
                    configBuilder.selectObjects(args[2])
                    configBuilder.setBuildProperties(false)
                    configBuilder.setBuildModules(false)
                }
                configBuilder.run()
                break
            default:
                println 'Wrong method! run "Main" for help'
                System.exit(1)
                break
        }
    }
}