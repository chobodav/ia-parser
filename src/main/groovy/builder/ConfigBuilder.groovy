package builder

import groovy.io.FileType
import groovy.xml.MarkupBuilder

/**
 * Created by d.chobotsky on 22.5.2015.
 */
class ConfigBuilder {
    static final NEW_LINE = System.getProperty("line.separator")
    static final END_LINE = '\\n\\'
    static final TAB = '\t'

    Properties appConfig
    File xmlConfigOutputFile
    String inputFolder
    def objectList
    List objectListToBuild
    def writer
    def builder

    // DEFAULTS
    boolean buildProperties = true
    boolean buildModules = true

    ConfigBuilder(String xmlConfig) {
        init(xmlConfig)
    }

    void run() {
        build()
    }

    void selectObjects(String objects) {
        objectList.each { String obj ->
            def objectId = obj.split('=')[1]
            if (objects.contains(objectId)) objectListToBuild << obj
        }
    }

    private void init(String xml) {
        xmlConfigOutputFile = new File(xml)
        new File(xmlConfigOutputFile.getParent()).mkdirs()

        appConfig = new Properties()
        new File("./config/app.properties").withInputStream {
            stream -> appConfig.load(stream)
        }

        this.setInputFolder(appConfig.getProperty('dir.builder.input'))
        this.setBuildProperties(appConfig.getProperty('builder.run.properties').toBoolean())
        this.setBuildModules(appConfig.getProperty('builder.run.modules').toBoolean())

        writer = new FileWriter(xmlConfigOutputFile)
        builder = new MarkupBuilder(writer)

        objectList = []
        objectListToBuild = []
        def objectFile = new File(inputFolder + 'objects/objectOrder.txt')
        objectFile.eachLine { line ->
            objectList << line
        }
    }

    private void build() {
        def dateNow = new Date().format('dd.MM.yyyy HH:mm:ss')
        def (serverVersion, version) = getVersionData()
        println 'building ... '

        builder.setDoubleQuotes(true);
        builder.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8");
        builder.paconfig(date: dateNow, server_version: serverVersion, version: version) {

            // Automation properties
            if (buildProperties) {
                def propertiesFileList = []
                def propertiesDir = new File(inputFolder + '/properties')
                propertiesDir.eachFileRecurse(FileType.FILES) { file ->
                    propertiesFileList << file
                }

                configFiles {
                    propertiesFileList.each {
                        properties(id: it.name, it.getText())
                    }
                }
            }

            // Automation Objects
            objects {
                if (objectListToBuild.isEmpty()) objectListToBuild = objectList
                // if object is not specified then export all

                objectListToBuild.each { obj ->
                    def objectClass = obj.split('=')[0]
                    def objectId = obj.split('=')[1]
                    println '[object] ' + objectId

                    o(class: objectClass, id: objectId) {
                        String objectFolder = inputFolder + 'objects/' + objectClass + '/' + objectId
                        'as' {
                            def attributesFile = new File(objectFolder + '/attributes.properties')

                            attributesFile.eachLine { line ->
                                def isEmpty = line.endsWith('=')
                                def attrName = line.split('=')[0]
                                def attrValues = line.substring(line.indexOf('=') + 1).split(';;')

                                if (isEmpty) {
                                    a(empty: true, name: attrName)
                                } else {
                                    a(name: attrName) {
                                        attrValues.each { value ->
                                            if (value.equals('##empty##')) value = ''
                                            v(value)
                                        }
                                    }
                                }
                            }
                        }

                        if (!(objectClass.equals('filter') || objectClass.equals('queue'))) {
                            def propContent = ''
                            def scriptDir = new File(objectFolder)
                            scriptDir.eachFile(FileType.FILES) { scriptFile ->
                                if (scriptFile.path.endsWith('.groovy')) {
                                    def scriptName = scriptFile.name.replaceFirst(~/\.[^\.]+$/, '')
                                    def firstLine
                                    def startLineIndex = 1
                                    scriptFile.withReader {
                                        firstLine = it.readLine()
                                        if (firstLine.contains('* CORE IA-tools *')) {
                                            while (true) {
                                                firstLine = it.readLine()
                                                startLineIndex++
                                                if (firstLine.contains('* CORE IA-tools *')) {
                                                    firstLine = it.readLine() // last empty line
                                                    startLineIndex++
                                                    break
                                                }
                                            }
                                            firstLine = it.readLine() // first script line
                                        }
                                    }

                                    def scriptImprove
                                    if (scriptName.equals('initScript')) {
                                        propContent = 'beforeProcessScript=' + firstLine + END_LINE + NEW_LINE
                                    } else {
                                        if (firstLine.contains('// LOCAL')) {
                                            propContent = 'scriptLocal_' + scriptName
                                            scriptImprove = propContent
                                            propContent += '='
                                        } else {
                                            propContent = 'script_' + scriptName
                                            scriptImprove = propContent
                                            propContent += '=' + firstLine + END_LINE + NEW_LINE
                                        }
                                        scriptImprove += '.improve=true'
                                    }

                                    scriptFile.eachLine(0) { line, index ->
                                        if (index > startLineIndex) {
                                            propContent += TAB
                                            if (line.length() > 0 && Character.isWhitespace(line.charAt(0))) propContent += '\\'
                                            propContent += line + END_LINE + NEW_LINE
                                        }
                                    }
                                    propContent += END_LINE + NEW_LINE + NEW_LINE

                                    if (scriptImprove != null) propContent += scriptImprove + NEW_LINE
                                }
                            }

                            def propertiesFile = new File(objectFolder + '/properties.properties')
                            if (propertiesFile.exists()) {
                                propContent += propertiesFile.text
                            }

                            properties(propContent)
                        }
                    }
                }
            }

            if (buildModules) {
                def moduleDir = new File(inputFolder + 'modules')
                modules {
                    moduleDir.eachFile(FileType.FILES) { moduleFile ->
                        String moduleName = moduleFile.getName().replaceFirst(~/\.[^\.]+$/, '')

                        module(id: moduleName) {
                            moduleConfig(moduleFile.text)
                        }
                    }

                }
            }
        }
    }

    private def getVersionData() {
        def versionFile = new File(inputFolder + '/version.txt')
        def serverVersion
        def version

        versionFile.eachLine { line ->
            def name = line.split('=')[0]
            def value = line.split('=')[1]

            switch (name) {
                case 'dateOfLastExport': break
                case 'serverVersion': serverVersion = value; break
                case 'version': version = value; break
                default: break
            }
        }

        return [serverVersion, version]
    }

    void setBuildProperties(boolean buildProperties) {
        this.buildProperties = buildProperties
    }

    void setBuildModules(boolean buildModules) {
        this.buildModules = buildModules
    }

    void setInputFolder(String inputFolder) {
        this.inputFolder = inputFolder
    }

}