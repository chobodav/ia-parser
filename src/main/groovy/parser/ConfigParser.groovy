package parser

import org.apache.commons.io.FileUtils

/**
 * Created by d.chobotsky on 5.7.2015.
 */
class ConfigParser {
    static final NEW_LINE = System.getProperty("line.separator")
    static final TAB = '\t'

    Properties appConfig
    Properties ignoreList
    String folderProject
    String folderOutputs
    def paConfig

    // DEFAULTS
    boolean runObjects = true
    boolean runProperties = true
    boolean runModules = true
    boolean useHeader = false

    ConfigParser(String xmlConfig) {
        init(xmlConfig)
    }

    void run() {
        parseVersionData()
        if (runProperties) parseServerProperties()
        if (runObjects) parseObjects()
        if (runModules) parseModules()
    }

    private void init(String xml) {
        File xmlConfigFile = new File(xml)
        paConfig = new XmlParser().parse(xmlConfigFile)

        appConfig = new Properties()
        new File("./config/app.properties").withInputStream {
            stream -> appConfig.load(stream)
        }

        this.setRunProperties(appConfig.getProperty('parser.run.properties').toBoolean())
        this.setRunObjects(appConfig.getProperty('parser.run.objects').toBoolean())
        this.setRunModules(appConfig.getProperty('parser.run.modules').toBoolean())
        this.setUseHeader(appConfig.getProperty('parser.run.script.useHeader').toBoolean())
        this.setFolderOutputs(appConfig.getProperty('dir.outputs'))

        ignoreList = new Properties()
        new File("./config/ignore.config").withInputStream {
            stream -> ignoreList.load(stream)
        }

        folderProject = folderOutputs + xmlConfigFile.getName().replaceFirst(~/\.[^\.]+$/, '')
        createFolders(folderProject)

        def resources = new File(folderProject + '/core')
        if (!resources.exists()) {
            FileUtils.copyDirectory(new File("../resources/core"), resources)
            FileUtils.copyDirectory(new File("../resources/doc"), new File(folderProject))
        }
    }

    private void parseVersionData() {
        def versionFile = createNewFile(folderProject + '/version.txt')
        versionFile << 'dateOfLastExport=' + paConfig.@date + NEW_LINE
        versionFile << 'serverVersion=' + paConfig.@server_version + NEW_LINE
        versionFile << 'version=' + paConfig.@version + NEW_LINE
    }

    private void parseServerProperties() {
        String folderName = folderProject + '/properties';
        paConfig.configFiles.properties.each { node ->
            println '[config] ' + node.@id
            createFolders(folderName)
            File propertiesFile = createNewFile(folderName + '/' + node.@id)
            propertiesFile << node.text()
        }
    }

    private void parseObjects() {
        String folderName = folderProject + '/objects';
        createFolders(folderName)
        File objectsFile = createNewFile(folderName + '/objectOrder.txt')

        int numberOfNodes = paConfig.objects.o.size()
        paConfig.objects.o.eachWithIndex { object, index ->
            showProgress('objects', index, numberOfNodes)
//            println 'Obj ' + object.@id
            String objectClass = object.@class
            String objectId = replaceSpecChars(object.@id)
            // Write object order
            objectsFile << objectClass + '=' + objectId + NEW_LINE

            String objectFolder = folderName + '/' + objectClass + '/' + objectId
            createFolders(objectFolder)
            // Write object attributes
            File attributeFile = createNewFile(objectFolder + '/attributes.properties')
            object.as.a.each { attribute ->
                String attributeName = attribute.@name

                if (ignoreList.containsKey(objectClass + '.' + attributeName)) return // e.g. server.current-time

                if (attribute.@empty) {
                    attributeFile << attributeName + '=' + NEW_LINE
                } else {
                    String attributeValue = ''
                    attribute.v.each { value ->
                        String val = (value.text()?.trim()) ? value.text() : '##empty##'
                        attributeValue += val + ';;'
                    }
                    if (attributeValue.endsWith(';;')) attributeValue = attributeValue.substring(0, attributeValue.length() - 2)
                    attributeFile << attributeName + '=' + attributeValue + NEW_LINE
                }
            }

            // Write object properties
            File propertiesFile = createNewFile(objectFolder + '/properties.properties')
            String objectProperties = object.properties.text()
            if (objectProperties) {
                File scriptHeaderTemp = new File('./resources/ia-tools-import.temp')
                File scriptFile
                boolean isScriptLine

                objectProperties.eachLine { line ->
                    boolean isNewScript = line.startsWith('script_')
                    boolean isNewScriptL = line.startsWith('scriptLocal_')
//                    boolean isNewProcess = line.startsWith('process_')
                    boolean isNewScriptB = line.startsWith('beforeProcessScript')

                    if (paConfig.@server_version.startsWith('8') && (isNewScript || isNewScriptL || isNewScriptB)) {
                        line = line.replace('\\n', NEW_LINE)
                        line = line.replace('\\t', TAB)
                        line = line.replace('&lt;', '<')
                        line = line.replace('&gt;', '>')
                        line = line.replace('\\\\', '\\')
                    }

                    if (!line.startsWith('\t')) isScriptLine = false
                    line = line.replace('\\n\\', '')

                    if (isNewScript || isNewScriptL) {
                        String scriptName = line.split('=')[0].substring(line.indexOf('_') + 1)
                        String splitLine = line.substring(line.indexOf('=') + 1)

                        if (scriptName.endsWith('.improve')) return // todo save improve=true

                        scriptFile = createNewFile(objectFolder + '/' + scriptName + '.groovy')
                        if (useHeader) scriptFile << scriptHeaderTemp.text
                        if (isNewScriptL) scriptFile << '// LOCAL' + NEW_LINE
                        if (splitLine.length() > 0) scriptFile << splitLine + NEW_LINE
                        isScriptLine = true
                    } else if (isNewScriptB) {
                        String splitLine = line.substring(line.indexOf('=') + 1)
                        scriptFile = createNewFile(objectFolder + '/initScript.groovy')
                        if (useHeader) scriptFile << scriptHeaderTemp.text
                        if (splitLine.length() > 0) scriptFile << splitLine + NEW_LINE
                        isScriptLine = true
//                    } else if (isNewProcess) {
//                        String scriptName = line.split('=')[0].substring(line.indexOf('_') + 1)
//                        String splitLine = line.split('=')
//                        if (scriptName.contains('.') && scriptFile != null) { // todo what if null store to temp
//                            scriptFile << line + NEW_LINE
//                        } else {
//                            scriptFile = createNewFile(objectFolder + '/' + scriptName + '.bat')
//                            if (splitLine.size() > 1) scriptFile << splitLine[1] + NEW_LINE
//                            scriptFile << NEW_LINE
//                        }
                    } else if (isScriptLine) {
                        line = line.replace(TAB, '')
                        while (line.startsWith('\\t')) {
                            line = line - '\\t'
                        }
                        if (!line.contains('\\\\t')) line = line.replace('\\t', '')
                        if (line.startsWith('\\')) line = line - '\\'

                        scriptFile << line + NEW_LINE
                    } else {
                        if (line.length() > 0) propertiesFile << line + NEW_LINE
                    }
                }
            }
        }
    }

    private void parseModules() {
        String folderName = folderProject + '/modules'
        int numberOfNodes = paConfig.modules.module.size()
        paConfig.modules.module.eachWithIndex { module, index ->
            showProgress('modules', index, numberOfNodes)
//            println 'Mod ' + module.@id
            createFolders(folderName)
            boolean isFirstLine = true
            File moduleFile
            String moduleFileName = module.@id
            String objectModule = module.moduleConfig.text()
            objectModule.eachLine { line ->
                boolean isXmlFile = line.startsWith('<')
                if (isFirstLine) {
                    moduleFileName += (isXmlFile) ? '.xml' : '.properties'
                    moduleFile = createNewFile(folderName + '/' + moduleFileName)
                    isFirstLine = false
                }
                moduleFile << line + NEW_LINE
            }
        }

        // Script Module
        createFolders(folderName + '/scriptModule')
        def scriptLibrary = new XmlParser().parse(new File(folderName + '/scriptModule.xml'))
        scriptLibrary.script.each { scriptNode ->
            def scriptFile = createNewFile(folderName + '/scriptModule/' + scriptNode.@name + '.groovy')
            scriptFile << '/*********************************' + NEW_LINE
            scriptFile << ' * Name: ' + scriptNode.@name + NEW_LINE
            scriptFile << ' * Package Name: ' + scriptNode.@packageName + NEW_LINE
            scriptFile << ' * Last Modified By: ' + scriptNode.@lastModifiedBy + NEW_LINE
            scriptFile << ' * Last Modified: ' + scriptNode.@lastModified + NEW_LINE
            scriptFile << ' * Description: ' + scriptNode.description.text() + NEW_LINE
            scriptFile << ' */' + NEW_LINE + NEW_LINE
            scriptFile << scriptNode.code.text()
        }

        // Scheduler Module
        createFolders(folderName + '/schedulerModule')
        def schedulerConfig = new XmlParser().parse(new File(folderName + '/schedulerModule.xml'))
        schedulerConfig.tasks.task.each { taskNode ->
            if (taskNode.@type.equals('script')) {
                File scriptFile = createNewFile(folderName + '/schedulerModule/' + taskNode.@name + '.groovy')
                scriptFile << '/*********************************' + NEW_LINE
                scriptFile << ' * Name: ' + taskNode.@name + NEW_LINE
                scriptFile << ' * Start: ' + taskNode.@start + NEW_LINE
                scriptFile << ' */' + NEW_LINE + NEW_LINE
                scriptFile << taskNode.script.text()
            }
        }

        // Event Module
        createFolders(folderName + '/eventModule')
        File eventFile = new File(folderName + '/eventModule.xml')
        if (eventFile.exists()) {
            def eventModule = new XmlParser().parse(eventFile)
            eventModule.handlerConfig.each { eventNode ->
                if (eventNode.worker.@class.contains('ScriptWorker')) {
                    File scriptFile = createNewFile(folderName + '/eventModule/' + eventNode.@name + '.groovy')
                    scriptFile << '/*********************************' + NEW_LINE
                    scriptFile << ' * Name: ' + eventNode.@name + NEW_LINE
                    scriptFile << ' * Type: ' + eventNode.@type + NEW_LINE
                    scriptFile << ' * Level: ' + eventNode.@level + NEW_LINE
                    scriptFile << ' */' + NEW_LINE + NEW_LINE
                    scriptFile << eventNode.worker.property.script.text()
                }
            }
        }

        // Attribute Module
        createFolders(folderName + '/attributeModule')
        def attributeModule = new XmlParser().parse(new File(folderName + '/attributeModule.xml'))
        List temp = new ArrayList()
        File attrFile = createNewFile(folderName + '/attributeModule/attributes.properties')

        attributeModule.attrTypes.attrType.each { attributeNode ->
            String type = attributeNode.paoTypes.value.text()
            String attrName = attributeNode.@name
            String attr = type + "." + attrName + ""
            attr += '=' + attributeNode.@type + NEW_LINE

            temp.add(attr)
        }

        File attributeFile;
        String tType;

        temp.sort()
        temp.each {
            attrFile << it

            def (tempType, tempAttr) = it.tokenize('.')
            if (!tempType.equals(tType)) {
                tType = tempType
//                println tType
                String attrFilePath = folderProject + '/core/properties/'
                createFolders(attrFilePath)
                attrFilePath += tType + '.properties'
                switch (tType) {
                    case ['server', 'job', 'document', 'input', 'device', 'filter', 'queue', 'jobdocument']:
                        attributeFile = createNewFile(attrFilePath)
                        break
                    default:
                        break
                }

            }
            switch (tType) {
                case ['server', 'job', 'document', 'input', 'device', 'filter', 'queue', 'jobdocument']:
                    attributeFile << tempAttr
                    break
                default:
                    break
            }
        }

        // Production Services Module
        createFolders(folderName + '/productionServices')
        def productionServices = new XmlParser().parse(new File(folderName + '/prodSModule.xml'))
        File scriptFile = createNewFile(folderName + '/productionServices/config.xml')
        scriptFile << productionServices.WebConfig.text()
    }

    private void createFolders(String path) {
        new File(path).mkdirs()
    }

    private File createNewFile(String filePath, boolean withRemove = true) {
        File file = new File(filePath)
        if (withRemove && file.exists()) file.delete()
        return file
    }

    private replaceSpecChars(String string) {
        String temp = string.replace('/', '_')
        temp = string.replace('\\', '_')
        temp = string.replace('&gt;', '_')
        temp = string.replace('&lt;', '_')
        temp = string.replace(':', '_')
        temp = string.replace('"', '_')
        temp = string.replace('|', '_')
        temp = string.replace('?', '_')
        temp = string.replace('*', '_')
        return temp
    }

    private void showProgress(String object, int index, int size) {
        if (index.equals(0)) print '[' + object + '] progress in % : '
        double done = (index + 1).intValue() / size
        int step = size / 8
        if (index % step == 0) print ' ' + (done * 100).intValue().toString() + ' '
        if (size.equals(index + 1)) println '100 done.'
    }

    void setRunObjects(boolean runObjects) {
        this.runObjects = runObjects
    }

    void setRunProperties(boolean runProperties) {
        this.runProperties = runProperties
    }

    void setRunModules(boolean runModules) {
        this.runModules = runModules
    }

    void setFolderOutputs(String folderOutputs) {
        this.folderOutputs = folderOutputs
    }

    void setUseHeader(boolean useHeader) {
        this.useHeader = useHeader
    }
}