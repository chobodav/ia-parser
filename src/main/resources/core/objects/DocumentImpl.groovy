import net.gmc.pa.attr.AttributeValue
import net.gmc.pa.exception.AttributeException
import net.gmc.pa.server.pao.AttributeListener
import net.gmc.pa.server.pao.Document
import net.gmc.pa.server.pao.Job
import net.gmc.pa.server.pao.Server
import net.omanager.common.exception.ObjectClassException

/**
 * Created by d.chobotsky on 30.6.2015.
 */
class DocumentImpl implements Document {


    @Override
    InputStream getInputStream() throws IOException {
        return null
    }

    @Override
    OutputStream createOutputStream() throws IOException {
        return null
    }

    @Override
    OutputStream createOutputStream(boolean b) throws IOException {
        return null
    }

    @Override
    void commitOutputStream() throws IOException {

    }

    @Override
    void commitWhenCancel() throws IOException {

    }

    @Override
    void rejectOutputStream() {

    }

    @Override
    File getDocumentFile() throws IOException {
        return null
    }

    @Override
    void replaceDocumentFile(File file) throws IOException {

    }

    @Override
    Job getJob() {
        return null
    }

    @Override
    int getCopyCount() {
        return 0
    }

    @Override
    boolean isFirstDocument() {
        return false
    }

    @Override
    boolean isLastDocument() {
        return false
    }

    @Override
    File getSpoolDir() throws FileNotFoundException {
        return null
    }

    @Override
    void setAttribute(String s) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String s1) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String[] strings) throws AttributeException {

    }

    @Override
    void setAttribute(String s, AttributeValue attributeValue) throws AttributeException, ObjectClassException {

    }

    @Override
    AttributeValue getAttribute(String s) {
        return null
    }

    @Override
    String getAttributeValue(String s) {
        return null
    }

    @Override
    String[] getAttributeValues(String s) {
        return new String[0]
    }

    @Override
    void addAttributeListener(AttributeListener attributeListener, String[] strings) {

    }

    @Override
    void addAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    void removeAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    String getId() {
        return null
    }

    @Override
    Server getServer() {
        return null
    }

    @Override
    String getObjectClass() {
        return null
    }
}
