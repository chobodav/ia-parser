import net.gmc.pa.attr.AttributeValue
import net.gmc.pa.exception.AttributeException
import net.gmc.pa.server.pao.AttributeListener
import net.gmc.pa.server.pao.Document
import net.gmc.pa.server.pao.Job
import net.gmc.pa.server.pao.Queue
import net.gmc.pa.server.pao.Server
import net.omanager.common.exception.InappropriateStateException
import net.omanager.common.exception.ObjectClassException
import net.omanager.common.util.Cleaner

import java.sql.SQLException

/**
 * Created by d.chobotsky on 30.6.2015.
 */
class JobImpl implements Job {



    @Override
    Document[] getDocuments() {
        return new Document[0]
    }

    @Override
    Document getDocument(int i) throws ArrayIndexOutOfBoundsException {
        return null
    }

    @Override
    Iterable<Document> documents() {
        return null
    }

    @Override
    String getJobId() {
        return null
    }

    @Override
    String getOwner() {
        return null
    }

    @Override
    void setMessage(String s) {

    }

    @Override
    InputStream getJobInputStream() throws InappropriateStateException, IOException {
        return null
    }

    @Override
    void writeJobStreamTo(OutputStream outputStream) throws InappropriateStateException, IOException {

    }

    @Override
    boolean isJobValid() {
        return false
    }

    @Override
    Queue getRequestedQueue() {
        return null
    }

    @Override
    void addCleaner(Cleaner cleaner) {

    }

    @Override
    void removeCleaner(Cleaner cleaner) {

    }

    @Override
    boolean isJobPaused() {
        return false
    }

    @Override
    boolean isCanceled() {
        return false
    }

    @Override
    void setStepForDP(int i) {

    }

    @Override
    int getStepForDP() {
        return 0
    }

    @Override
    boolean isJobDone() {
        return false
    }

    @Override
    Queue getActiveQueue() {
        return null
    }

    @Override
    String getCurrentJobState() {
        return null
    }

    @Override
    File getLogFile() {
        return null
    }

    @Override
    File beginAddDocument() throws InappropriateStateException {
        return null
    }

    @Override
    Document commitAddDocument(String s) throws InappropriateStateException, ObjectClassException, AttributeException, SQLException {
        return null
    }

    @Override
    void cancelAddDocument() {

    }

    @Override
    boolean cancel(String s) throws InappropriateStateException {
        return false
    }

    @Override
    void setAttribute(String s) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String s1) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String[] strings) throws AttributeException {

    }

    @Override
    void setAttribute(String s, AttributeValue attributeValue) throws AttributeException, ObjectClassException {

    }

    @Override
    AttributeValue getAttribute(String s) {
        return null
    }

    @Override
    String getAttributeValue(String s) {
        return null
    }

    @Override
    String[] getAttributeValues(String s) {
        return new String[0]
    }

    @Override
    void addAttributeListener(AttributeListener attributeListener, String[] strings) {

    }

    @Override
    void addAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    void removeAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    String getId() {
        return null
    }

    @Override
    Server getServer() {
        return null
    }

    @Override
    String getObjectClass() {
        return null
    }
}
