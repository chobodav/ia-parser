import net.gmc.pa.attr.AttributeValue
import net.gmc.pa.exception.AttributeException
import net.gmc.pa.server.pao.AttributeListener
import net.gmc.pa.server.pao.Server
import net.omanager.common.exception.InappropriateStateException
import net.omanager.common.exception.ObjectClassException


/**
 * Created by d.chobotsky on 30.6.2015.
 */
class ServerImpl implements Server {

    @Override
    void disable(String s) throws InappropriateStateException {

    }

    @Override
    void enable(String s) throws InappropriateStateException {

    }

    @Override
    void setAttribute(String s) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String s1) throws AttributeException {

    }

    @Override
    void setAttribute(String s, String[] strings) throws AttributeException {

    }

    @Override
    void setAttribute(String s, AttributeValue attributeValue) throws AttributeException, ObjectClassException {

    }

    @Override
    AttributeValue getAttribute(String s) {
        return null
    }

    @Override
    String getAttributeValue(String s) {
        return null
    }

    @Override
    String[] getAttributeValues(String s) {
        return new String[0]
    }

    @Override
    void addAttributeListener(AttributeListener attributeListener, String[] strings) {

    }

    @Override
    void addAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    void removeAttributeListener(AttributeListener attributeListener) {

    }

    @Override
    String getId() {
        return null
    }

    @Override
    Server getServer() {
        return null
    }

    @Override
    String getObjectClass() {
        return null
    }
}