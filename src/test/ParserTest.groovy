import org.junit.Test
import parser.ConfigParser

/**
 * Created by d.chobotsky on 23.7.2015.
 */
class ParserTest {

    final String TEST_OUTPUTS = 'output/test/'

    @Test
    void sampleWorkflowParseTest() {
        String filePath = 'samples/AmFam_IA.xml'
        ConfigParser configParser = new ConfigParser(filePath)
        configParser.setFolderOutputs(TEST_OUTPUTS)
        configParser.run()
    }

}